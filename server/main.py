#!/usr/bin/env python3
import time
import sched
from server import MyHTTPServer as HTTPServer
from server import RequestHandler
from optparse import OptionParser


if __name__ == '__main__':
    parser = OptionParser()
    parser.add_option("-p", "--port", type="int", dest="port", default=8000)

    options, args = parser.parse_args()

    server_address = ("127.0.0.1", options.port)
    server = HTTPServer(
        server_address,
        RequestHandler
    )

    server.take_fee()

    schedule = sched.scheduler(
        time.time,
        time.sleep,
    )
    schedule.enter(10, 1, server.take_fee, (schedule, ), )
    schedule.run()

    try:
        print(f'Server is up on port: { options.port }')
        server.serve_forever()
    except KeyboardInterrupt:
        pass

    server.server_close()
    print('Server is down')
