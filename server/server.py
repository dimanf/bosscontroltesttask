#!/usr/bin/env python3
from random import randrange
from http.server import HTTPServer
from http.server import BaseHTTPRequestHandler
from routes import routes
# from functools import partial


class MyHTTPServer(HTTPServer):
    def __init__(self, server_address, RequestHandlerClass, **kwargs):
        self.balance = randrange(20 * 10 * 10 * 10)
        super().__init__(
            server_address,
            # partial(RequestHandlerClass, self),
            RequestHandlerClass,
            **kwargs
        )

    def get_balance(self):
        """
        The method gets client's balance
        """
        return self.balance

    def set_balance(self, amount):
        """
        The method sets slient's balance
        """
        self.balance = amount

    def get_fees_sum(self):
        """
        The method calculates a fees sum according to
        some loginc
        """
        # TODO: Make fees caluculateing instead of random value
        return randrange(20 * 10 * 10)

    def take_fee(self, schedule=None):
        """
        The method takes clients fee from its balance
        """
        self.balance -= self.get_fees_sum()
        print(f"Feed balance: {self.balance}\n")
        if schedule:
            schedule.enter(10, 1, self.take_fee, (schedule, ))
            schedule.run()


class RequestHandler(BaseHTTPRequestHandler):
    """
    A simple RequestHandler implementation
    """

    def __init__(self, server, *args, **kwargs):
        self.routes = routes
        self.server = server
        super().__init__(*args, **kwargs)

    def get_balance(self):
        """
        The method gets client's balance
        """
        return self.server.get_balance()

    def set_balance(self, amount):
        """
        The method sets slient's balance
        """
        self.server.set_balance(amount)

    def do_GET(self):
        self.respond()

    def handle_http(self, status, content_type):
        if self.path in self.routes:
            content_type = "text/html"
            response_content = f"Your balance is: {self.get_balance()}"
        else:
            content_type = "text/plain"
            response_content = "404 Not Found\n"

        self.send_response(status)
        self.send_header('Content-type', content_type)
        self.end_headers()
        return bytes(response_content, "UTF-8")

    def respond(self):
        content = self.handle_http(200, 'text/html')
        self.wfile.write(content)
