# !/usr/bin/env python3
import time
import sched
from client import Client


if __name__ == '__main__':
    clinet = Client()

    # Creacte shcedule instance in further we'll use it for
    # requesting balance
    schedule = sched.scheduler(
        time.time,
        time.sleep,
    )

    schedule.enter(5, 1, clinet.get_balance, (schedule, ))

    try:
        schedule.run()
    except KeyboardInterrupt:
        pass
