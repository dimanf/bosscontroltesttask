# !/usr/bin/env python3
import urllib.request


class Client:
    """
    The cline that requesting servers according to schedule
    """

    def get_balance(self, schedule):

        request = urllib.request.Request('http://127.0.0.1:8082/get_balance')
        with urllib.request.urlopen(request) as response:
            result = response.read()
            print(f"Response[{response.code}], {result}")

        schedule.enter(5, 1, self.get_balance, (schedule, ))

    def set_balance(self):
        raise NotImplementedError()
